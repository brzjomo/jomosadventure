package com.brzjomo.jomosadventure.helper;

import com.brzjomo.jomosadventure.JomosAdventure;
import com.brzjomo.jomosadventure.datagenerator.ItemModelGenerator;
import com.brzjomo.jomosadventure.init.ModItems;
import net.minecraft.item.Item;
import net.minecraft.util.Identifier;

public class ModelGeneratorHelper {
    public static String getProperModelJson(Identifier idIn, String modelJson) {
        boolean isDone = false;
        for (Item i : ModItems.BLOCKITEM_LIST) {
//            JomosAdventure.LOGGER.info("1: " + ModItems.getId(i).getPath() + " 2: " + idIn.getPath() + " 3: " + idIn.getPath().contains(ModItems.getPath(i)));
            if (idIn.getPath().contains(ModItems.getPath(i))) {
                modelJson = ItemModelGenerator.createItemModelJson(idIn, "block");
                isDone = true;
            }
        }

        if (!isDone) {
            for (Item i : ModItems.HELD_ITEM_LIST) {
                if (idIn.getPath().contains(ModItems.getPath(i))) {
                    modelJson = ItemModelGenerator.createItemModelJson(idIn, "handheld");
                    isDone = true;
                }
            }
        }

        if (!isDone) {
            modelJson = ItemModelGenerator.createItemModelJson(idIn, "generated");
        }

        return modelJson;
    }
}
