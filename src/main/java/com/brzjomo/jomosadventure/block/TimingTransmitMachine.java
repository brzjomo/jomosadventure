package com.brzjomo.jomosadventure.block;

import com.brzjomo.jomosadventure.JomosAdventure;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.LiteralText;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

import java.util.Random;

public class TimingTransmitMachine extends Block {
    public TimingTransmitMachine(Settings settings) {
        super(settings);
    }
    private static int ticks = 0;

    @Override
    public void onPlaced(World world, BlockPos pos, BlockState state, @Nullable LivingEntity placer, ItemStack itemStack) {
        if (!world.isClient){
            placer.sendSystemMessage(new LiteralText("方块已放置"), placer.getUuid());
        }
        super.onPlaced(world, pos, state, placer, itemStack);
    }

    @Override
    public void scheduledTick(BlockState state, ServerWorld world, BlockPos pos, Random random) {
//        world.getTimeOfDay();
        while (ticks == 20 ) {
            JomosAdventure.LOGGER.info(ticks + " : " + world.getTimeOfDay());
            ticks = 0;
        }

        if (world.getTimeOfDay() == 6030){
            JomosAdventure.LOGGER.info("午时已过");
        }

        super.scheduledTick(state, world, pos, random);
    }
}
