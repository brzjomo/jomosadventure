package com.brzjomo.jomosadventure.init;

import com.brzjomo.jomosadventure.JomosAdventure;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.Identifier;

public class ModSoundEvents extends SoundEvents {
    //Sound Path
    public static final String NEUTRAL_CLOCK_TICK_PATH = "neutral.clock_tick";

    //SoundEvent
    public static final SoundEvent NEUTRAL_CLOCK_TICK = new SoundEvent(new Identifier(JomosAdventure.MOD_ID, NEUTRAL_CLOCK_TICK_PATH));

    //SoundEvent List
    public static final SoundEvent[] SOUND_EVENTS_LIST = {NEUTRAL_CLOCK_TICK};

    //Get Path
    public static String getPath(SoundEvent soundEventIn) {
        if (soundEventIn.equals(NEUTRAL_CLOCK_TICK)) {
            return NEUTRAL_CLOCK_TICK_PATH;
        } else {
            return null;
        }
    }

    //Get ID
    public static Identifier getId(SoundEvent soundEventIn) {
        return new Identifier(JomosAdventure.MOD_ID, getPath(soundEventIn));
    }
}
