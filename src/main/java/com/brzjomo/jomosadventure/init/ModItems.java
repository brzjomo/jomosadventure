package com.brzjomo.jomosadventure.init;

import com.brzjomo.jomosadventure.JomosAdventure;
import com.brzjomo.jomosadventure.item.*;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.Rarity;

public class ModItems {
    //Item
    public static final TestItem TEST_ITEM = new TestItem(new Item.Settings().rarity(Rarity.RARE).maxDamage(32).group(ModItemGroups.TEST_GROUP));
    public static final Test2Item TEST_2_ITEM = new Test2Item(new Item.Settings().rarity(Rarity.RARE).maxDamage(128).group(ModItemGroups.TEST_GROUP));
    public static final Item SILVER_INGOT = new Item(new Item.Settings().group(ItemGroup.MATERIALS));
    public static final Item SILVER_NUGGET = new Item(new Item.Settings().group(ItemGroup.MATERIALS));
    public static final Item MITHRIL_INGOT = new Item(new Item.Settings().group(ItemGroup.MATERIALS));
    public static final Item MITHRIL_NUGGET = new Item(new Item.Settings().group(ItemGroup.MATERIALS));
    public static final TimingTeleportGem TIMING_TELEPORT_GEM = new TimingTeleportGem(6100, new Item.Settings().rarity(Rarity.EPIC).fireproof().group(ModItemGroups.TEST_GROUP));
    public static final TimingTeleportGem NIGHTFALL_TELEPORT_GEM = new TimingTeleportGem(12000, new Item.Settings().rarity(Rarity.EPIC).maxDamage(16).fireproof().group(ItemGroup.MATERIALS));
    public static final BlessTeleportGem BLESS_TELEPORT_GEM = new BlessTeleportGem(0, new Item.Settings().rarity(Rarity.EPIC).maxDamage(16).fireproof().group(ItemGroup.MATERIALS));
    public static final StaffOfDesignator STAFF_OF_DESIGNATOR = new StaffOfDesignator(new Item.Settings().rarity(Rarity.UNCOMMON).maxDamage(666).group(ModItemGroups.TEST_GROUP));

    //Block Item
    public static final BlockItem SILVER_ORE_ITEM = new BlockItem(ModBlocks.SILVER_ORE_BLOCK, new Item.Settings().group(ItemGroup.BUILDING_BLOCKS));
    public static final BlockItem SILVER_BLOCK_ITEM = new BlockItem(ModBlocks.SILVER_BLOCK, new Item.Settings().group(ItemGroup.BUILDING_BLOCKS));
    public static final BlockItem MITHRIL_BLOCK_ITEM = new BlockItem(ModBlocks.MITHRIL_BLOCK, new Item.Settings().group(ItemGroup.BUILDING_BLOCKS));
    public static final BlockItem SLOPE_STONE_ITEM = new BlockItem(ModBlocks.SLOPE_STONE, new Item.Settings().group(ItemGroup.BUILDING_BLOCKS));

    //Item List
    public static final Item[] ITEM_LIST = {TEST_ITEM, TEST_2_ITEM, SILVER_INGOT, SILVER_NUGGET, MITHRIL_INGOT, MITHRIL_NUGGET, TIMING_TELEPORT_GEM, NIGHTFALL_TELEPORT_GEM, BLESS_TELEPORT_GEM, STAFF_OF_DESIGNATOR};
    public static final Item[] HELD_ITEM_LIST = {};
    public static final Item[] BLOCKITEM_LIST = {SILVER_ORE_ITEM, SILVER_BLOCK_ITEM, MITHRIL_BLOCK_ITEM, SLOPE_STONE_ITEM};

    //Item Path
    public static final String TEST_ITEM_PATH = "test_item";
    public static final String TEST_2_ITEM_PATH = "test_2_item";
    public static final String SILVER_INGOT_PATH = "silver_ingot";
    public static final String SILVER_NUGGET_PATH = "silver_nugget";
    public static final String MITHRIL_INGOT_PATH = "mithril_ingot";
    public static final String MITHRIL_NUGGET_PATH = "mithril_nugget";
    public static final String TIMING_TRANSMIT_GEM_PATH = "timing_teleport_gem";
    public static final String NIGHTFALL_TELEPORT_GEM_PATH = "nightfall_teleport_gem";
    public static final String BLESS_TELEPORT_GEM_PATH = "bless_teleport_gem";
    public static final String STAFF_OF_DESIGNATOR_PATH = "staff_of_designator";

    //Get Path
    public static String getPath(Item itemIn) {
        if (itemIn.equals(TEST_ITEM)) {
            return TEST_ITEM_PATH;
        } else if (itemIn.equals(TEST_2_ITEM)) {
            return TEST_2_ITEM_PATH;
        } else if (itemIn.equals(SILVER_INGOT)) {
            return SILVER_INGOT_PATH;
        } else if (itemIn.equals(SILVER_NUGGET)) {
            return SILVER_NUGGET_PATH;
        } else if (itemIn.equals(SILVER_ORE_ITEM)) {
            return ModBlocks.SILVER_ORE_PATH;
        } else if (itemIn.equals(SILVER_BLOCK_ITEM)) {
            return ModBlocks.SILVER_BLOCK_PATH;
        } else if (itemIn.equals(MITHRIL_BLOCK_ITEM)) {
            return ModBlocks.MITHRIL_BLOCK_PATH;
        } else if (itemIn.equals(MITHRIL_INGOT)) {
            return MITHRIL_INGOT_PATH;
        } else if (itemIn.equals(MITHRIL_NUGGET)) {
            return MITHRIL_NUGGET_PATH;
        } else if (itemIn.equals(TIMING_TELEPORT_GEM)) {
            return TIMING_TRANSMIT_GEM_PATH;
        } else if (itemIn.equals(NIGHTFALL_TELEPORT_GEM)) {
            return NIGHTFALL_TELEPORT_GEM_PATH;
        } else if (itemIn.equals(BLESS_TELEPORT_GEM)) {
            return BLESS_TELEPORT_GEM_PATH;
        } else if (itemIn.equals(SLOPE_STONE_ITEM)) {
            return ModBlocks.SLOPE_STONE_PATH;
        } else if (itemIn.equals(STAFF_OF_DESIGNATOR)) {
            return ModItems.STAFF_OF_DESIGNATOR_PATH;
        } else {
            return null;
        }
    }

    //Get ID
    public static Identifier getId(Item itemIn) {
        return new Identifier(JomosAdventure.MOD_ID, getPath(itemIn));
    }

}
