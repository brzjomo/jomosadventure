package com.brzjomo.jomosadventure.init;

import com.brzjomo.jomosadventure.JomosAdventure;
import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;

public class ModItemGroups {
    public static final ItemGroup TEST_GROUP = FabricItemGroupBuilder.build(
            new Identifier(JomosAdventure.MOD_ID, "test"),
            () -> new ItemStack(ModItems.TEST_ITEM));
}
