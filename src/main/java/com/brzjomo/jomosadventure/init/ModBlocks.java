package com.brzjomo.jomosadventure.init;

import com.brzjomo.jomosadventure.JomosAdventure;
import com.brzjomo.jomosadventure.block.SlopeBlock;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.Material;
import net.minecraft.block.OreBlock;
import net.minecraft.util.Identifier;

public class ModBlocks {
    //Block
    public static final Block SILVER_ORE_BLOCK = new OreBlock(FabricBlockSettings.of(Material.STONE).strength(3.0F, 4.0F).requiresTool());
    public static final Block SILVER_BLOCK = new Block(FabricBlockSettings.of(Material.METAL).strength(2.0F, 4.0F).requiresTool());
    public static final Block MITHRIL_BLOCK = new Block(FabricBlockSettings.of(Material.METAL).strength(50.0F, 100.0F).requiresTool());
    public static final SlopeBlock SLOPE_STONE = new SlopeBlock(Blocks.STONE.getDefaultState(), FabricBlockSettings.copyOf(Blocks.STONE).requiresTool());

    //BlockList
    public static final Block[] BLOCK_LIST = {SILVER_ORE_BLOCK, SILVER_BLOCK, MITHRIL_BLOCK, SLOPE_STONE};

    //Block Path
    public static final String SILVER_ORE_PATH = "silver_ore";
    public static final String SILVER_BLOCK_PATH = "silver_block";
    public static final String MITHRIL_BLOCK_PATH = "mithril_block";
    public static final String SLOPE_STONE_PATH = "slope_stone";

    //Get Block ID
    public static Identifier getId(Block blockIn) {
        if (blockIn.equals(SILVER_ORE_BLOCK)) {
            return new Identifier(JomosAdventure.MOD_ID, SILVER_ORE_PATH);
        } else if (blockIn.equals(SILVER_BLOCK)) {
            return new Identifier(JomosAdventure.MOD_ID, SILVER_BLOCK_PATH);
        } else if (blockIn.equals(MITHRIL_BLOCK)) {
            return new Identifier(JomosAdventure.MOD_ID, MITHRIL_BLOCK_PATH);
        } else if (blockIn.equals(SLOPE_STONE)) {
            return new Identifier(JomosAdventure.MOD_ID, SLOPE_STONE_PATH);
        } else {
            return null;
        }

    }

}
