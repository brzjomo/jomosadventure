package com.brzjomo.jomosadventure.init;

import com.brzjomo.jomosadventure.JomosAdventure;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.RegistryEntry;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.gen.YOffset;
import net.minecraft.world.gen.feature.*;
import net.minecraft.world.gen.placementmodifier.CountPlacementModifier;
import net.minecraft.world.gen.placementmodifier.HeightRangePlacementModifier;
import net.minecraft.world.gen.placementmodifier.SquarePlacementModifier;

import java.util.Arrays;

public class ModFeatures {
    // Ore Configured Feature Key
    public static final RegistryKey<ConfiguredFeature<?, ?>> SILVER_ORE_CFK_OVERWORLD = RegistryKey.of(Registry.CONFIGURED_FEATURE_KEY,
            new Identifier(JomosAdventure.MOD_ID, "silver_ore_overworld"));

    // Ore Placed Feature Key
    public static final RegistryKey<PlacedFeature> SILVER_ORE_PFK_OVERWORLD = RegistryKey.of(Registry.PLACED_FEATURE_KEY,
            new Identifier(JomosAdventure.MOD_ID, "silver_ore_overworld"));

    // Ore Configured Feature Key List
    public static final RegistryKey[] ORE_CONFIGURED_FEATURE_KEY_LIST_OVERWORLD = {SILVER_ORE_CFK_OVERWORLD};

    // Ore Placed Feature Key List
    public static final RegistryKey[] ORE_PLACED_FEATURE_KEY_LIST_OVERWORLD = {SILVER_ORE_PFK_OVERWORLD};

    // Ore Configured Feature
    public static final ConfiguredFeature<?, ?> SILVER_ORE_CONFIGURED_FEATURE_OVERWORLD = new ConfiguredFeature<>(
            Feature.ORE, new OreFeatureConfig(
            OreConfiguredFeatures.STONE_ORE_REPLACEABLES,
            ModBlocks.SILVER_ORE_BLOCK.getDefaultState(),
            5)); // vein size

    // Ore Placed Feature
    public static final PlacedFeature SILVER_ORE_PLACED_FEATURE_OVERWORLD = new PlacedFeature(
            RegistryEntry.of(SILVER_ORE_CONFIGURED_FEATURE_OVERWORLD),
            Arrays.asList(
                    CountPlacementModifier.of(12), // number of veins per chunk
                    SquarePlacementModifier.of(), // spreading horizontally
                    HeightRangePlacementModifier.uniform(YOffset.getBottom(), YOffset.fixed(40))
            )); // height

    // Ore Configured Feature List
    public static final ConfiguredFeature<?, ?>[] ORE_CONFIGURED_FEATURE_LIST_OVERWORLD = {SILVER_ORE_CONFIGURED_FEATURE_OVERWORLD};

    // Ore Placed Feature List
    public static final PlacedFeature[] ORE_PLACED_FEATURE_LIST_OVERWORLD = {SILVER_ORE_PLACED_FEATURE_OVERWORLD};

}
