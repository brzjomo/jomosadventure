package com.brzjomo.jomosadventure.init;

import com.brzjomo.jomosadventure.JomosAdventure;
import com.google.common.collect.Lists;
import com.google.gson.JsonObject;
import net.minecraft.item.Items;
import net.minecraft.util.Identifier;

import static com.brzjomo.jomosadventure.datagenerator.CreateRecipeJson.*;

public class ModItemRecipes {
    public static JsonObject TEST_ITEM_RECIPE = createShapedRecipeJson(
            Lists.newArrayList("item", "item"),
            Lists.newArrayList(new Identifier("book"), new Identifier("wheat")),
            Lists.newArrayList(
                    'A',
                    'B'
            ),
            Lists.newArrayList(
                    "AB ",
                    "   ",
                    "   "
            ),
            1,
            new Identifier(JomosAdventure.MOD_ID, ModItems.TEST_ITEM_PATH)
    );

    public static JsonObject TEST_2_ITEM_RECIPE = createShapedRecipeJson(
            Lists.newArrayList("item", "item"),
            Lists.newArrayList(new Identifier(JomosAdventure.MOD_ID, ModItems.TEST_ITEM_PATH), new Identifier("water_bucket")),
            Lists.newArrayList(
                    'A',
                    'B'
            ),
            Lists.newArrayList(
                    "AB ",
                    "   ",
                    "   "
            ),
            1,
            new Identifier(JomosAdventure.MOD_ID, ModItems.TEST_2_ITEM_PATH)
    );

    public static JsonObject SILVER_INGOT_SMELTING = createSmeltingRecipeJson(
            Lists.newArrayList("tag"),
            Lists.newArrayList(new Identifier("c", "silver_ores")),
            0.9F,
            133,
            new Identifier(JomosAdventure.MOD_ID, ModItems.SILVER_INGOT_PATH)
    );

    public static JsonObject SILVER_NUGGET = createShapedRecipeJson(
            Lists.newArrayList("tag"),
            Lists.newArrayList(new Identifier("c", "silver_ingots")),
            Lists.newArrayList('A'),
            Lists.newArrayList(
                    "A  ",
                    "   ",
                    "   "
            ),
            9,
            new Identifier(JomosAdventure.MOD_ID, ModItems.SILVER_NUGGET_PATH)
    );

    public static JsonObject SILVER_INGOT_FROM_NUGGET = createShapedRecipeJson(
            Lists.newArrayList("tag"),
            Lists.newArrayList(new Identifier("c", "silver_nuggets")),
            Lists.newArrayList('A'),
            Lists.newArrayList(
                    "AAA",
                    "AAA",
                    "AAA"
            ),
            1,
            new Identifier(JomosAdventure.MOD_ID, ModItems.SILVER_INGOT_PATH)
    );

    public static JsonObject SILVER_INGOT = createShapedRecipeJson(
            Lists.newArrayList("tag"),
            Lists.newArrayList(new Identifier("c", "silver_blocks")),
            Lists.newArrayList('A'),
            Lists.newArrayList(
                    "A  ",
                    "   ",
                    "   "
            ),
            9,
            new Identifier(JomosAdventure.MOD_ID, ModItems.SILVER_INGOT_PATH)
    );

    public static JsonObject SILVER_BLOCK = createShapedRecipeJson(
            Lists.newArrayList("tag"),
            Lists.newArrayList(new Identifier("c", "silver_ingots")),
            Lists.newArrayList('A'),
            Lists.newArrayList(
                    "AAA",
                    "AAA",
                    "AAA"
            ),
            1,
            new Identifier(JomosAdventure.MOD_ID, ModBlocks.SILVER_BLOCK_PATH)
    );

    public static JsonObject MITHRIL_NUGGET = createShapedRecipeJson(
            Lists.newArrayList("tag"),
            Lists.newArrayList(new Identifier("c", "mithril_ingots")),
            Lists.newArrayList('A'),
            Lists.newArrayList(
                    "A  ",
                    "   ",
                    "   "
            ),
            9,
            new Identifier(JomosAdventure.MOD_ID, ModItems.MITHRIL_NUGGET_PATH)
    );

    public static JsonObject MITHRIL_INGOT_FROM_NUGGET = createShapedRecipeJson(
            Lists.newArrayList("tag", "tag"),
            Lists.newArrayList(new Identifier("c", "silver_nuggets"), new Identifier("c", "gold_nuggets")),
            Lists.newArrayList('A', 'B'),
            Lists.newArrayList(
                    "AAA",
                    "ABA",
                    "AAA"
            ),
            1,
            new Identifier(JomosAdventure.MOD_ID, ModItems.MITHRIL_INGOT_PATH)
    );

    public static JsonObject MITHRIL_INGOT_FROM_NUGGET_2 = createShapedRecipeJson(
            Lists.newArrayList("tag"),
            Lists.newArrayList(new Identifier("c", "mithril_nuggets")),
            Lists.newArrayList('A'),
            Lists.newArrayList(
                    "AAA",
                    "AAA",
                    "AAA"
            ),
            1,
            new Identifier(JomosAdventure.MOD_ID, ModItems.MITHRIL_INGOT_PATH)
    );

    public static JsonObject MITHRIL_INGOT = createShapedRecipeJson(
            Lists.newArrayList("tag"),
            Lists.newArrayList(new Identifier("c", "mithril_blocks")),
            Lists.newArrayList('A'),
            Lists.newArrayList(
                    "A  ",
                    "   ",
                    "   "
            ),
            9,
            new Identifier(JomosAdventure.MOD_ID, ModItems.MITHRIL_INGOT_PATH)
    );

    public static JsonObject MITHRIL_BLOCK = createShapedRecipeJson(
            Lists.newArrayList("tag"),
            Lists.newArrayList(new Identifier("c", "mithril_ingots")),
            Lists.newArrayList('A'),
            Lists.newArrayList(
                    "AAA",
                    "AAA",
                    "AAA"
            ),
            1,
            new Identifier(JomosAdventure.MOD_ID, ModBlocks.MITHRIL_BLOCK_PATH)
    );

    public static JsonObject NIGHTFALL_TELEPORT_GEM = createShapedRecipeJson(
            Lists.newArrayList("item", "item", "item", "item", "tag"),
            Lists.newArrayList(new Identifier("ender_pearl"), new Identifier("clock"), new Identifier("emerald"), new Identifier("ender_eye"), new Identifier("beds")),
            Lists.newArrayList('A', 'B', 'C', 'D', 'E'),
            Lists.newArrayList(
                    "ABA",
                    "CDC",
                    "AEA"
            ),
            1,
            new Identifier(JomosAdventure.MOD_ID, ModItems.NIGHTFALL_TELEPORT_GEM_PATH)
    );

    public static JsonObject BLESS_TELEPORT_GEM = createShapedRecipeJson(
            Lists.newArrayList("item", "item", "item", "item", "item"),
            Lists.newArrayList(new Identifier("ender_pearl"), new Identifier("clock"), new Identifier("emerald"), new Identifier("ender_eye"), new Identifier("golden_apple")),
            Lists.newArrayList('A', 'B', 'C', 'D', 'E'),
            Lists.newArrayList(
                    "ABA",
                    "CDC",
                    "AEA"
            ),
            1,
            new Identifier(JomosAdventure.MOD_ID, ModItems.BLESS_TELEPORT_GEM_PATH)
    );

    public static JsonObject STAFF_OF_DESIGNATOR = createShapedRecipeJson(
            Lists.newArrayList("item", "tag"),
            Lists.newArrayList(new Identifier("ender_pearl"), new Identifier("c", "mithril_ingots")),
            Lists.newArrayList('A', 'B'),
            Lists.newArrayList(
                    "BBA",
                    " BB",
                    "B  "
            ),
            1,
            new Identifier(JomosAdventure.MOD_ID, ModItems.STAFF_OF_DESIGNATOR_PATH)
    );

    public static JsonObject SLOPE_STONE_FROM_STONECUTTING = createStoneCuttingRecipeJson(
            Lists.newArrayList("item"),
            Lists.newArrayList(new Identifier("stone")),
            2,
            new Identifier(JomosAdventure.MOD_ID, ModBlocks.SLOPE_STONE_PATH)
    );

    //Additional Recipe For Vanilla Object
    public static JsonObject SHEARS_FROM_COPPER_INGOT = createShapedRecipeJson(
            Lists.newArrayList("item"),
            Lists.newArrayList(new Identifier("copper_ingot")),
            Lists.newArrayList('A'),
            Lists.newArrayList(
                    "A  ",
                    " A ",
                    "   "
            ),
            1,
            new Identifier("minecraft", "shears")
    );

    public static JsonObject BUCKET_FROM_COPPER_INGOT = createShapedRecipeJson(
            Lists.newArrayList("item"),
            Lists.newArrayList(new Identifier("copper_ingot")),
            Lists.newArrayList('A'),
            Lists.newArrayList(
                    "A A",
                    " A ",
                    "   "
            ),
            1,
            new Identifier("minecraft", "bucket")
    );

    public static JsonObject HOPPER_FROM_COPPER_INGOT = createShapedRecipeJson(
            Lists.newArrayList("item", "item"),
            Lists.newArrayList(new Identifier("copper_ingot"), new Identifier("chest")),
            Lists.newArrayList('A', 'B'),
            Lists.newArrayList(
                    "A A",
                    "ABA",
                    " A "
            ),
            1,
            new Identifier("minecraft", "hopper")
    );
}
