package com.brzjomo.jomosadventure.datagenerator;

import com.brzjomo.jomosadventure.JomosAdventure;
import net.minecraft.util.Identifier;

public class ItemModelGenerator {
    public static String createItemModelJson(Identifier id, String type) {
        if ("generated".equals(type) || "handheld".equals(type)) {
            //The two types of items. "handheld" is used mostly for tools and the like, while "generated" is used for everything else.
            return "{\n" +
                    "  \"parent\": \"item/" + type + "\",\n" +
                    "  \"textures\": {\n" +
                    "    \"layer0\": \"" + JomosAdventure.MOD_ID + ":" + id.getPath() + "\"\n" +
                    "  }\n" +
                    "}";
        } else if ("block".equals(type)) {
//            JomosAdventure.LOGGER.info("BlockItem Model: " + JomosAdventure.MOD_ID + ":" + id.getPath());
            //However, if the item is a block-item, it will have a different model json than the previous two.
            return "{\n" + "  \"parent\": \"" + JomosAdventure.MOD_ID + ":block/" + id.getPath().substring(5) + "\"\n" + "}";
        } else {
            //If the type is invalid, return an empty json string.
            return "";
        }
    }
}
