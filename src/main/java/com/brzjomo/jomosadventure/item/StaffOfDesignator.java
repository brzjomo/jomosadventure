package com.brzjomo.jomosadventure.item;

import com.brzjomo.jomosadventure.JomosAdventure;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;

public class StaffOfDesignator extends Item {
    public StaffOfDesignator(Settings settings) {
        super(settings);
    }

    public static BlockState targetBlock_Basic[] = {Blocks.WATER.getDefaultState()};
    public static BlockState targetBlock_L1[] = {Blocks.WATER.getDefaultState(), Blocks.DIRT.getDefaultState(), Blocks.GRASS_BLOCK.getDefaultState()};

    @Override
    public ActionResult useOnBlock(ItemUsageContext context) {
        World world = context.getWorld();
        PlayerEntity player = context.getPlayer();
        BlockPos blockPos = context.getBlockPos();
        BlockState air = Blocks.AIR.getDefaultState();
        BlockState stone = Blocks.STONE.getDefaultState();
        BlockState water = Blocks.WATER.getDefaultState();
        BlockState lava = Blocks.LAVA.getDefaultState();
        BlockState targetBlocks[] = targetBlock_Basic;
        boolean isSuccess = false;
        boolean isLavaTarget = false;
        // shapeMode根据附魔切换，双层double，平面矩形rectangle，立方体cube
        String shapeMode = "single";
        // range也可以用附魔改变
        int range;
        if(!player.isSneaking()) {
            range = 5;
        } else {
            range = 2;
        }

        // cool down
        player.getItemCooldownManager().set(this, 60);

        if (shapeMode == "single" && player != null) {
            if (player.getHorizontalFacing().equals(Direction.NORTH)) {
                if (!world.isClient()) {
                    for (int i = 0; i < range; i++) {
                        for (BlockState j : targetBlocks) {
                            if (world.getBlockState(blockPos.add(0, 0, -i)).equals(j)) {
                                world.setBlockState(blockPos.add(0, 0, -i), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                                isSuccess = true;
                            }
                        }
                        if (isLavaTarget && world.getBlockState(blockPos.add(0, 0, -i)).equals(lava)) {
                            world.setBlockState(blockPos.add(0, 0, -i), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            isSuccess = true;
                        }
                    }
                }
                if (world.isClient()) {
                    context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                }
                if (isSuccess) {
                    return ActionResult.SUCCESS;
                } else {
                    return ActionResult.PASS;
                }
            } else if(player.getHorizontalFacing().equals(Direction.SOUTH)) {
                if (!world.isClient()) {
                    for (int i = 0; i < range; i++) {
                        for (BlockState j : targetBlocks) {
                            if (world.getBlockState(blockPos.add(0, 0, i)).equals(j)) {
                                world.setBlockState(blockPos.add(0, 0, i), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                                isSuccess = true;
                            }
                        }
                        if (isLavaTarget && world.getBlockState(blockPos.add(0, 0, i)).equals(lava)) {
                            world.setBlockState(blockPos.add(0, 0, i), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            isSuccess = true;
                        }
                    }
                }
                if (world.isClient()) {
                    context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                }
                if (isSuccess) {
                    return ActionResult.SUCCESS;
                } else {
                    return ActionResult.PASS;
                }
            } else if(player.getHorizontalFacing().equals(Direction.EAST)) {
                if (!world.isClient()) {
                    for (int i = 0; i < range; i++) {
                        for (BlockState j : targetBlocks) {
                            if (world.getBlockState(blockPos.add(i, 0, 0)).equals(j)) {
                                world.setBlockState(blockPos.add(i, 0, 0), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                                isSuccess = true;
                            }
                        }
                        if (isLavaTarget && world.getBlockState(blockPos.add(i, 0, 0)).equals(lava)) {
                            world.setBlockState(blockPos.add(i, 0, 0), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            isSuccess = true;
                        }
                    }
                }
                if (world.isClient()) {
                    context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                }
                if (isSuccess) {
                    return ActionResult.SUCCESS;
                } else {
                    return ActionResult.PASS;
                }
            } else {
                if (!world.isClient()) {
                    for (int i = 0; i < range; i++) {
                        for (BlockState j : targetBlocks) {
                            if (world.getBlockState(blockPos.add(-i, 0, 0)).equals(j)) {
                                world.setBlockState(blockPos.add(-i, 0, 0), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                                isSuccess = true;
                            }
                        }
                        if (isLavaTarget && world.getBlockState(blockPos.add(-i, 0, 0)).equals(lava)) {
                            world.setBlockState(blockPos.add(-i, 0, 0), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            isSuccess = true;
                        }
                    }
                }
                if (world.isClient()) {
                    context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                }
                if (isSuccess) {
                    return ActionResult.SUCCESS;
                } else {
                    return ActionResult.PASS;
                }
            }
        }

        return ActionResult.PASS;
    }
}
