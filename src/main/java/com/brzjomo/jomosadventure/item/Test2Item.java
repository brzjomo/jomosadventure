package com.brzjomo.jomosadventure.item;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class Test2Item extends Item {
    private static final boolean LIMITED_MODE = false;
    private static final boolean WITH_LAVA_MODE = true;

    public Test2Item(Settings settings) {
        super(settings);
    }

    @Override
    public ActionResult useOnBlock(ItemUsageContext context) {
        World world = context.getWorld();
        PlayerEntity player = context.getPlayer();
        BlockPos blockPos = context.getBlockPos();
        BlockState water = Blocks.WATER.getDefaultState();
        BlockState lava = Blocks.LAVA.getDefaultState();
        // -Z为北，+Z为南，-X为西，+X为东
        BlockState stateAbove = world.getBlockState(context.getBlockPos().add(0, 1, 0));
        BlockState stateNorth = world.getBlockState(context.getBlockPos().add(0, 0, -1));
        BlockState stateSouth = world.getBlockState(context.getBlockPos().add(0, 0, +1));
        BlockState stateEast = world.getBlockState(context.getBlockPos().add(1, 0, 0));
        BlockState stateWest = world.getBlockState(context.getBlockPos().add(-1, 0, 0));
        boolean isCornerWater = false;
        boolean isCornerLava = false;
        if (!player.isSneaking()) {
            if (player.getHorizontalFacing().equals(Direction.NORTH)) {
                if (stateAbove.equals(water)) {
                    if(!world.isClient()){
                        world.setBlockState(blockPos.add(0, 1, 0), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                        if (player != null) {
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                        }
                    }
                    if(world.isClient()) {
                        context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                    }
                    return ActionResult.success(world.isClient);
                } else if (stateSouth.equals(water)) {
                    if(!world.isClient()) {
                        world.setBlockState(blockPos.add(0, 0, 1), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                        if (player != null) {
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                        }
                    }
                    if(world.isClient()){
                        context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                    }
                    return ActionResult.success(world.isClient);
                } else if (stateNorth.equals(water)) {
                    if(!world.isClient()) {
                        world.setBlockState(blockPos.add(0, 0, -1), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                        if (player != null) {
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                        }
                    }
                    if(world.isClient()){
                        context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                    }
                    return ActionResult.success(world.isClient);
                } else if (stateWest.equals(water)) {
                    if(!world.isClient()) {
                        world.setBlockState(blockPos.add(-1, 0, 0), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                        if (player != null) {
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                        }
                    }
                    if(world.isClient()){
                        context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                    }
                    return ActionResult.success(world.isClient);
                } else if (stateEast.equals(water)) {
                    if(!world.isClient()) {
                        world.setBlockState(blockPos.add(1, 0, 0), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                        if (player != null) {
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                        }
                    }
                    if(world.isClient()){
                        context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                    }
                    return ActionResult.success(world.isClient);
                } else {
                    if (world.getBlockState(context.getBlockPos().add(-1, 0, 1)).equals(water)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(-1, 0, 1), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        isCornerWater = true;
                    }
                    if (world.getBlockState(context.getBlockPos().add(1, 0, 1)).equals(water)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(1, 0, 1), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        isCornerWater = true;
                    }
                    if (world.getBlockState(context.getBlockPos().add(-1, 0, -1)).equals(water)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(-1, 0, -1), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        isCornerWater = true;
                    }
                    if (world.getBlockState(context.getBlockPos().add(1, 0, -1)).equals(water)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(1, 0, -1), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        isCornerWater = true;
                    }
                    if(isCornerWater) {
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    }
                }

                // lava
                if (WITH_LAVA_MODE) {
                    if (stateAbove.equals(lava)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(0, 1, 0), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    } else if (stateSouth.equals(lava)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(0, 0, 1), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    } else if (stateNorth.equals(lava)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(0, 0, -1), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    } else if (stateWest.equals(lava)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(-1, 0, 0), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    } else if (stateEast.equals(lava)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(1, 0, 0), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    } else {
                        if (world.getBlockState(context.getBlockPos().add(-1, 0, 1)).equals(lava)) {
                            if(!world.isClient()) {
                                world.setBlockState(blockPos.add(-1, 0, 1), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                                if (player != null) {
                                    context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                                }
                            }
                            isCornerLava = true;
                        }
                        if (world.getBlockState(context.getBlockPos().add(1, 0, 1)).equals(lava)) {
                            if(!world.isClient()) {
                                world.setBlockState(blockPos.add(1, 0, 1), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                                if (player != null) {
                                    context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                                }
                            }
                            isCornerLava = true;
                        }
                        if (world.getBlockState(context.getBlockPos().add(-1, 0, -1)).equals(lava)) {
                            if(!world.isClient()) {
                                world.setBlockState(blockPos.add(-1, 0, -1), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                                if (player != null) {
                                    context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                                }
                            }
                            isCornerLava = true;
                        }
                        if (world.getBlockState(context.getBlockPos().add(1, 0, -1)).equals(lava)) {
                            if(!world.isClient()) {
                                world.setBlockState(blockPos.add(1, 0, -1), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                                if (player != null) {
                                    context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                                }
                            }
                            isCornerLava = true;
                        }
                        if(isCornerLava) {
                            if(world.isClient()){
                                context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                            }
                            return ActionResult.success(world.isClient);
                        }
                    }
                }
            } else if (player.getHorizontalFacing().equals(Direction.SOUTH)) {
                if (stateAbove.equals(water)) {
                    if(!world.isClient()) {
                        world.setBlockState(blockPos.add(0, 1, 0), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                        if (player != null) {
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                        }
                    }
                    if(world.isClient()){
                        context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                    }
                    return ActionResult.success(world.isClient);
                } else if (stateNorth.equals(water)) {
                    if(!world.isClient()) {
                        world.setBlockState(blockPos.add(0, 0, -1), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                        if (player != null) {
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                        }
                    }
                    if(world.isClient()){
                        context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                    }
                    return ActionResult.success(world.isClient);
                } else if (stateSouth.equals(water)) {
                    if(!world.isClient()) {
                        world.setBlockState(blockPos.add(0, 0, 1), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                        if (player != null) {
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                        }
                    }
                    if(world.isClient()){
                        context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                    }
                    return ActionResult.success(world.isClient);
                } else if (stateEast.equals(water)) {
                    if(!world.isClient()) {
                        world.setBlockState(blockPos.add(1, 0, 0), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                        if (player != null) {
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                        }
                    }
                    if(world.isClient()){
                        context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                    }
                    return ActionResult.success(world.isClient);
                } else if (stateWest.equals(water)) {
                    if(!world.isClient()) {
                        world.setBlockState(blockPos.add(-1, 0, 0), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                        if (player != null) {
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                        }
                    }
                    if(world.isClient()){
                        context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                    }
                    return ActionResult.success(world.isClient);
                } else {
                    if (world.getBlockState(context.getBlockPos().add(-1, 0, 1)).equals(water)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(-1, 0, 1), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        isCornerWater = true;
                    }
                    if (world.getBlockState(context.getBlockPos().add(1, 0, 1)).equals(water)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(1, 0, 1), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        isCornerWater = true;
                    }
                    if (world.getBlockState(context.getBlockPos().add(-1, 0, -1)).equals(water)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(-1, 0, -1), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        isCornerWater = true;
                    }
                    if (world.getBlockState(context.getBlockPos().add(1, 0, -1)).equals(water)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(1, 0, -1), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        isCornerWater = true;
                    }
                    if(isCornerWater) {
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    }
                }

                // lava
                if (WITH_LAVA_MODE) {
                    if (stateAbove.equals(lava)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(0, 1, 0), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    } else if (stateNorth.equals(lava)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(0, 0, -1), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    } else if (stateSouth.equals(lava)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(0, 0, 1), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    } else if (stateEast.equals(lava)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(1, 0, 0), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    } else if (stateWest.equals(lava)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(-1, 0, 0), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    } else {
                        if (world.getBlockState(context.getBlockPos().add(-1, 0, 1)).equals(lava)) {
                            if(!world.isClient()) {
                                world.setBlockState(blockPos.add(-1, 0, 1), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                                if (player != null) {
                                    context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                                }
                            }
                            isCornerLava = true;
                        }
                        if (world.getBlockState(context.getBlockPos().add(1, 0, 1)).equals(lava)) {
                            if(!world.isClient()) {
                                world.setBlockState(blockPos.add(1, 0, 1), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                                if (player != null) {
                                    context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                                }
                            }
                            isCornerLava = true;
                        }
                        if (world.getBlockState(context.getBlockPos().add(-1, 0, -1)).equals(lava)) {
                            if(!world.isClient()) {
                                world.setBlockState(blockPos.add(-1, 0, -1), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                                if (player != null) {
                                    context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                                }
                            }
                            isCornerLava = true;
                        }
                        if (world.getBlockState(context.getBlockPos().add(1, 0, -1)).equals(lava)) {
                            if(!world.isClient()) {
                                world.setBlockState(blockPos.add(1, 0, -1), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                                if (player != null) {
                                    context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                                }
                            }
                            isCornerLava = true;
                        }
                        if(isCornerLava) {
                            if(world.isClient()){
                                context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                            }
                            return ActionResult.success(world.isClient);
                        }
                    }
                }
            } else if (player.getHorizontalFacing().equals(Direction.EAST)) {
                if (stateAbove.equals(water)) {
                    if(!world.isClient()) {
                        world.setBlockState(blockPos.add(0, 1, 0), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                        if (player != null) {
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                        }
                    }
                    if(world.isClient()){
                        context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                    }
                    return ActionResult.success(world.isClient);
                } else if (stateWest.equals(water)) {
                    if(!world.isClient()) {
                        world.setBlockState(blockPos.add(-1, 0, 0), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                        if (player != null) {
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                        }
                    }
                    if(world.isClient()){
                        context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                    }
                    return ActionResult.success(world.isClient);
                } else if (stateEast.equals(water)) {
                    if(!world.isClient()) {
                        world.setBlockState(blockPos.add(1, 0, 0), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                        if (player != null) {
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                        }
                    }
                    if(world.isClient()){
                        context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                    }
                    return ActionResult.success(world.isClient);
                } else if (stateNorth.equals(water)) {
                    if(!world.isClient()) {
                        world.setBlockState(blockPos.add(0, 0, -1), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                        if (player != null) {
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                        }
                    }
                    if(world.isClient()){
                        context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                    }
                    return ActionResult.success(world.isClient);
                } else if (stateSouth.equals(water)) {
                    if(!world.isClient()) {
                        world.setBlockState(blockPos.add(0, 0, 1), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                        if (player != null) {
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                        }
                    }
                    if(world.isClient()){
                        context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                    }
                    return ActionResult.success(world.isClient);
                } else {
                    if (world.getBlockState(context.getBlockPos().add(-1, 0, 1)).equals(water)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(-1, 0, 1), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        isCornerWater = true;
                    }
                    if (world.getBlockState(context.getBlockPos().add(1, 0, 1)).equals(water)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(1, 0, 1), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        isCornerWater = true;
                    }
                    if (world.getBlockState(context.getBlockPos().add(-1, 0, -1)).equals(water)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(-1, 0, -1), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        isCornerWater = true;
                    }
                    if (world.getBlockState(context.getBlockPos().add(1, 0, -1)).equals(water)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(1, 0, -1), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        isCornerWater = true;
                    }
                    if(isCornerWater) {
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    }
                }

                // lava
                if (WITH_LAVA_MODE) {
                    if (stateAbove.equals(lava)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(0, 1, 0), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    } else if (stateWest.equals(lava)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(-1, 0, 0), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    } else if (stateEast.equals(lava)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(1, 0, 0), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    } else if (stateNorth.equals(lava)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(0, 0, -1), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    } else if (stateSouth.equals(lava)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(0, 0, 1), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    } else {
                        if (world.getBlockState(context.getBlockPos().add(-1, 0, 1)).equals(lava)) {
                            if(!world.isClient()) {
                                world.setBlockState(blockPos.add(-1, 0, 1), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                                if (player != null) {
                                    context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                                }
                            }
                            isCornerLava = true;
                        }
                        if (world.getBlockState(context.getBlockPos().add(1, 0, 1)).equals(lava)) {
                            if(!world.isClient()) {
                                world.setBlockState(blockPos.add(1, 0, 1), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                                if (player != null) {
                                    context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                                }
                            }
                            isCornerLava = true;
                        }
                        if (world.getBlockState(context.getBlockPos().add(-1, 0, -1)).equals(lava)) {
                            if(!world.isClient()) {
                                world.setBlockState(blockPos.add(-1, 0, -1), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                                if (player != null) {
                                    context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                                }
                            }
                            isCornerLava = true;
                        }
                        if (world.getBlockState(context.getBlockPos().add(1, 0, -1)).equals(lava)) {
                            if(!world.isClient()) {
                                world.setBlockState(blockPos.add(1, 0, -1), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                                if (player != null) {
                                    context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                                }
                            }
                            isCornerLava = true;
                        }
                        if(isCornerLava) {
                            if(world.isClient()){
                                context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                            }
                            return ActionResult.success(world.isClient);
                        }
                    }
                }
            } else if (player.getHorizontalFacing().equals(Direction.WEST)) {
                if (stateAbove.equals(water)) {
                    if(!world.isClient()) {
                        world.setBlockState(blockPos.add(0, 1, 0), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                        if (player != null) {
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                        }
                    }
                    if(world.isClient()){
                        context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                    }
                    return ActionResult.success(world.isClient);
                } else if (stateEast.equals(water)) {
                    if(!world.isClient()) {
                        world.setBlockState(blockPos.add(1, 0, 0), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                        if (player != null) {
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                        }
                    }
                    if(world.isClient()){
                        context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                    }
                    return ActionResult.success(world.isClient);
                } else if (stateWest.equals(water)) {
                    if(!world.isClient()) {
                        world.setBlockState(blockPos.add(-1, 0, 0), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                        if (player != null) {
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                        }
                    }
                    if(world.isClient()){
                        context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                    }
                    return ActionResult.success(world.isClient);
                } else if (stateSouth.equals(water)) {
                    if(!world.isClient()) {
                        world.setBlockState(blockPos.add(0, 0, 1), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                        if (player != null) {
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                        }
                    }
                    if(world.isClient()){
                        context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                    }
                    return ActionResult.success(world.isClient);
                } else if (stateNorth.equals(water)) {
                    if(!world.isClient()) {
                        world.setBlockState(blockPos.add(0, 0, -1), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                        if (player != null) {
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                        }
                    }
                    if(world.isClient()){
                        context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                    }
                    return ActionResult.success(world.isClient);
                } else {
                    if (world.getBlockState(context.getBlockPos().add(-1, 0, 1)).equals(water)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(-1, 0, 1), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        isCornerWater = true;
                    }
                    if (world.getBlockState(context.getBlockPos().add(1, 0, 1)).equals(water)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(1, 0, 1), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        isCornerWater = true;
                    }
                    if (world.getBlockState(context.getBlockPos().add(-1, 0, -1)).equals(water)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(-1, 0, -1), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        isCornerWater = true;
                    }
                    if (world.getBlockState(context.getBlockPos().add(1, 0, -1)).equals(water)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(1, 0, -1), Blocks.STONE.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        isCornerWater = true;
                    }
                    if(isCornerWater) {
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    }
                }

                // lava
                if (WITH_LAVA_MODE) {
                    if (stateAbove.equals(lava)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(0, 1, 0), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    } else if (stateEast.equals(lava)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(1, 0, 0), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    } else if (stateWest.equals(lava)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(-1, 0, 0), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    } else if (stateSouth.equals(lava)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(0, 0, 1), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    } else if (stateNorth.equals(lava)) {
                        if(!world.isClient()) {
                            world.setBlockState(blockPos.add(0, 0, -1), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    } else {
                        if (world.getBlockState(context.getBlockPos().add(-1, 0, 1)).equals(lava)) {
                            if(!world.isClient()) {
                                world.setBlockState(blockPos.add(-1, 0, 1), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                                if (player != null) {
                                    context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                                }
                            }
                            isCornerLava = true;
                        }
                        if (world.getBlockState(context.getBlockPos().add(1, 0, 1)).equals(lava)) {
                            if(!world.isClient()) {
                                world.setBlockState(blockPos.add(1, 0, 1), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                                if (player != null) {
                                    context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                                }
                            }
                            isCornerLava = true;
                        }
                        if (world.getBlockState(context.getBlockPos().add(-1, 0, -1)).equals(lava)) {
                            if(!world.isClient()) {
                                world.setBlockState(blockPos.add(-1, 0, -1), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                                if (player != null) {
                                    context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                                }
                            }
                            isCornerLava = true;
                        }
                        if (world.getBlockState(context.getBlockPos().add(1, 0, -1)).equals(lava)) {
                            if(!world.isClient()) {
                                world.setBlockState(blockPos.add(1, 0, -1), Blocks.OBSIDIAN.getDefaultState(), Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                                if (player != null) {
                                    context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                                }
                            }
                            isCornerLava = true;
                        }
                        if(isCornerLava) {
                            if(world.isClient()){
                                context.getPlayer().playSound(SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, 1.0F, 1.0F);
                            }
                            return ActionResult.success(world.isClient);
                        }
                    }
                }
            }
        } else {
            if (LIMITED_MODE) {
                // 转化为物品时删去将石头转化为水的功能，只在此测试物品中保留
                if (world.getBlockState(blockPos).equals(Blocks.STONE.getDefaultState())) {
                    if(!world.isClient()) {
                        world.setBlockState(context.getBlockPos(), water, Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                        if (player != null) {
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                        }
                    }
                    if(world.isClient()){
                        context.getPlayer().playSound(SoundEvents.BLOCK_WATER_AMBIENT, 1.0F, 1.0F);
                    }
                    return ActionResult.success(world.isClient);
                } else if (world.getBlockState(blockPos).equals(Blocks.OBSIDIAN.getDefaultState())) {
                    if(!world.isClient()) {
                        world.setBlockState(context.getBlockPos(), lava, Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                        if (player != null) {
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                        }
                    }
                    if(world.isClient()){
                        context.getPlayer().playSound(SoundEvents.BLOCK_LAVA_AMBIENT, 1.0F, 1.0F);
                    }
                    return ActionResult.success(world.isClient);
                }
            } else {
                if (WITH_LAVA_MODE) {
                    if (world.getBlockState(blockPos).equals(Blocks.OBSIDIAN.getDefaultState())) {
                        if(!world.isClient()) {
                            world.setBlockState(context.getBlockPos(), lava, Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_LAVA_AMBIENT, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    } else {
                        if(!world.isClient()) {
                            world.setBlockState(context.getBlockPos(), water, Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                            if (player != null) {
                                context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                            }
                        }
                        if(world.isClient()){
                            context.getPlayer().playSound(SoundEvents.BLOCK_WATER_AMBIENT, 1.0F, 1.0F);
                        }
                        return ActionResult.success(world.isClient);
                    }
                } else {
                    if(!world.isClient()) {
                        world.setBlockState(context.getBlockPos(), water, Block.NOTIFY_ALL | Block.REDRAW_ON_MAIN_THREAD);
                        if (player != null) {
                            context.getStack().damage(1, player, p -> p.sendToolBreakStatus(context.getHand()));
                        }
                    }
                    if(world.isClient()){
                        context.getPlayer().playSound(SoundEvents.BLOCK_WATER_AMBIENT, 1.0F, 1.0F);
                    }
                    return ActionResult.success(world.isClient);
                }
            }
        }

        return ActionResult.PASS;
    }

    @Override
    public void appendTooltip(ItemStack stack, @Nullable World world, List<Text> tooltip, TooltipContext context) {
        tooltip.add(new TranslatableText(stack.getTranslationKey() + ".tooltip." + "1").formatted(Formatting.DARK_AQUA).formatted(Formatting.ITALIC));
        tooltip.add(new TranslatableText(stack.getTranslationKey() + ".tooltip." + "2").formatted(Formatting.DARK_GRAY));
//        tooltip.add(new TranslatableText(stack.getTranslationKey() + ".tooltip." + "3").formatted(Formatting.DARK_RED));
    }
}
