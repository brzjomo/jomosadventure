package com.brzjomo.jomosadventure.item;

import com.brzjomo.jomosadventure.JomosAdventure;
import com.brzjomo.jomosadventure.init.ModItems;
import com.brzjomo.jomosadventure.init.ModSoundEvents;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;

public class TimingTeleportGem extends Item {
    public static int dayLength = 24000;
    //调试用
    private static int timec = 0;
    public long targetedTime;
    public long realTime;
    private boolean isSentMessage = false;
    private boolean isPlayedA = false;
    private boolean isPlayedB = false;

    public TimingTeleportGem(long time, Settings settings) {
        super(settings);
        this.targetedTime = time;
    }

    @Override
    public Text getName() {
        return new TranslatableText(this.getTranslationKey(), getDayTimeInClock(realTime));
    }

    @Override
    public Text getName(ItemStack stack) {
        return new TranslatableText(this.getTranslationKey(stack), getDayTimeInClock(realTime));
    }

    //

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        ItemStack itemStack = user.getStackInHand(hand);
        int maxDamage = itemStack.getMaxDamage();
        NbtCompound nbt = itemStack.getOrCreateNbt();

        //item usage cool down
        user.getItemCooldownManager().set(this, 100);

        if (user.isSneaking()) {
            if (world.isClient()) {
                if (!nbt.contains("Damage") || nbt.getInt("Damage") < maxDamage - 1) {
                    user.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_PICKUP, SoundCategory.PLAYERS, 0.8F, 0.4F);
                }
            }

            if (!world.isClient()) {
                if (!nbt.contains("Damage") || nbt.getInt("Damage") < maxDamage - 1) {
                    writePositionToNbt(nbt, user.getBlockPos());
                    writeWorldRegistryToNbt(nbt, user.getWorld());

                    //override original nbt
                    itemStack.getOrCreateNbt().copyFrom(nbt);
//                isUsed = true;

                    //damage item
                    user.getStackInHand(Hand.MAIN_HAND).damage(1, user, (p) -> {
                        p.sendToolBreakStatus(user.getActiveHand());
                    });

                    //调试用
//                if (JomosAdventure.ISDEBUG) {
//                    JomosAdventure.LOGGER.info("isUsed: " + isUsed);
//                    JomosAdventure.LOGGER.info("isUsed: " + compoundNbt.toString());
//                    JomosAdventure.LOGGER.info(printPositionFromNbt(nbt));
//                }

//            user.sendMessage(new TranslatableText("item." + JomosAdventure.MOD_ID + "." + "timing_teleport_gem" + ".message." + "1"), false);
                    return TypedActionResult.success(itemStack);
                }
            }
        }
        return TypedActionResult.pass(itemStack);
    }

    public void writePositionToNbt(NbtCompound nbt, BlockPos pos) {
        nbt.putInt("PinnedPos_X", pos.getX());
        nbt.putInt("PinnedPos_Y", pos.getY());
        nbt.putInt("PinnedPos_Z", pos.getZ());
    }

    public BlockPos readPositionFromNbt(NbtCompound nbt) {
        return new BlockPos(nbt.getInt("PinnedPos_X"), nbt.getInt("PinnedPos_Y"), nbt.getInt("PinnedPos_Z"));
    }

    public String printPositionFromNbt(NbtCompound nbt) {
        return "X: " + nbt.getInt("PinnedPos_X") + " , Y: " + nbt.getInt("PinnedPos_Y") + " , Z: " + nbt.getInt("PinnedPos_Z");
    }

    public String printNBT(NbtCompound nbt) {
        return nbt.toString();
    }

    public void writeWorldRegistryToNbt(NbtCompound nbt, World world) {
        nbt.putString("PinnedWorldRegistry", world.getRegistryKey().toString());
    }

    public Boolean isSameWorld(NbtCompound nbt, PlayerEntity player) {
        return player.getWorld().getRegistryKey().toString().equals(nbt.getString("PinnedWorldRegistry"));
    }

    public long getDay(long time) {
        return time / dayLength;
    }

    public long getDayPlusOne(long time) {
        return time / dayLength + 1;
    }

    public long getDayTime(long time) {
        return time % dayLength;
    }

    public String getDayTimeInClock(long time) {
        double tickPerHour = 1000;
        double tickPerMinute = tickPerHour / 60;

        double hour = Math.floor(getDayTime(time) / tickPerHour);
        double minute = Math.floor((getDayTime(time) - tickPerHour * hour) / tickPerMinute);

        //调试用
//        if (JomosAdventure.ISDEBUG) {
//            JomosAdventure.LOGGER.info("hour: " + hour + " , minute: " + minute + " , daytime: " + getDayTime(time));
//        }

        if (hour < 18) {
            hour += 6;
        } else {
            hour -= 18;
        }

        int trimHour = (int) hour;
        int trimMinute = (int) minute;

        return formatNumForClock(trimHour) + ":" + formatNumForClock(trimMinute);
    }

    public String formatNumForClock(int num) {
        if (num < 10) {
            return "0" + num;
        } else {
            return "" + num;
        }
    }

    public String printCurrentTime(long time) {
        return "第 " + getDayPlusOne(time) + " 天，" + getDayTimeInClock(time) + " (24小时制)";
    }

    @Override
    public void inventoryTick(ItemStack stack, World world, Entity entity, int slot, boolean selected) {
        realTime = world.getTimeOfDay();
        //只要宝石在身上，不用放在主手就可以得到提示。
        //消息提示代码，在聊天栏向持有者发送提醒
        if (!world.isClient()) {
            if (getDayTime(realTime) == getDayTime(targetedTime) - 20 * 1) {
                isSentMessage = false;
                isPlayedA = false;
                isPlayedB = false;
            }

            if (getDayTime(realTime) == getDayTime(targetedTime) && (stack.isItemEqual(new ItemStack(ModItems.NIGHTFALL_TELEPORT_GEM)) || stack.isItemEqual(new ItemStack(ModItems.TIMING_TELEPORT_GEM))) && !isSentMessage) {
                //调试用
                if (JomosAdventure.ISDEBUG) {
                    JomosAdventure.LOGGER.info("传送时间: " + printCurrentTime(realTime) + " , 玩家名称: " + entity.getEntityName());
                }

                String notice_1 = "item." + JomosAdventure.MOD_ID + "." + "timing_teleport_gem" + ".notice." + "1";
                Objects.requireNonNull(world.getClosestPlayer(entity, 1)).sendMessage(new TranslatableText(notice_1, getDayPlusOne(realTime), getDayTimeInClock(realTime)), false);
                isSentMessage = true;
            }
        }

        if (world.isClient()) {
            if (getDayTime(realTime) == getDayTime(targetedTime) - 20 * 26 && !isPlayedA) {
                world.playSoundFromEntity((PlayerEntity) entity, entity, ModSoundEvents.NEUTRAL_CLOCK_TICK, SoundCategory.NEUTRAL, 1.0F, 0.4F);
                isPlayedA = true;
            }

            if (getDayTime(realTime) == getDayTime(targetedTime) - 20 * 8 && !isPlayedB) {
                entity.playSound(SoundEvents.BLOCK_PORTAL_TRIGGER, 0.2F, 0.4F);
                isPlayedB = true;
            }
        }

        //传送实现
        if (stack.isItemEqual(new ItemStack(ModItems.TIMING_TELEPORT_GEM)) && selected || stack.isItemEqual(new ItemStack(ModItems.NIGHTFALL_TELEPORT_GEM)) && selected) {
            NbtCompound nbt = stack.getOrCreateNbt();

            //调试用
//            timec++;
//            if (JomosAdventure.ISDEBUG) {
//                if (timec % 300 == 0) {
//                    JomosAdventure.LOGGER.info("Logger: " + newNbt.toString());
//                }
//
//                if (timec % 20 == 0) {
//                    JomosAdventure.LOGGER.info("realtime: " + realTime + " , 当前时间: " + getDayTimeInClock(realTime));
//                }
//            }

            if (nbt.contains("PinnedPos_X") && isSameWorld(nbt, (PlayerEntity) entity)) {
                if (!world.isClient()) {
                    if (getDayTime(realTime) == getDayTime(targetedTime)) {
                        BlockPos pinnedPosition = readPositionFromNbt(nbt);

                        //调试用
//                    if (JomosAdventure.ISDEBUG) {
//                        JomosAdventure.LOGGER.info("尝试传送的地点:" + pinnedPosition.toString());
//                    }

//                        world.playSound(null, entity.getX(), entity.getY(), entity.getZ(), SoundEvents.BLOCK_END_PORTAL_SPAWN, SoundCategory.NEUTRAL, 0.5F, 0.4F / (world.getRandom().nextFloat() * 0.4F + 0.8F));

                        entity.requestTeleport(pinnedPosition.getX() + 0.5D, pinnedPosition.getY() + 1.0D, pinnedPosition.getZ() + 0.5D);
//                    world.getClosestPlayer(entity, 1).sendMessage(new TranslatableText("item." + JomosAdventure.MOD_ID + "." + "timing_teleport_gem" + ".message." + "4"), false);
                    }
                }

                // 传送成功音效
                if (world.isClient()) {
                    if (getDayTime(realTime) == getDayTime(targetedTime)) {
                        entity.playSound(SoundEvents.BLOCK_END_PORTAL_SPAWN, 0.4F, 0.4F);
                    }
                }
            }

            if (!world.isClient() && !nbt.contains("PinnedPos_X")) {
                if (getDayTime(realTime) == getDayTime(targetedTime)) {
                    Objects.requireNonNull(world.getClosestPlayer(entity, 1)).sendMessage(new TranslatableText("item." + JomosAdventure.MOD_ID + "." + "timing_teleport_gem" + ".error." + "1"), false);
                }
            }

            if (nbt.contains("PinnedPos_X") && !isSameWorld(nbt, (PlayerEntity) entity)) {
                if (!world.isClient()) {
                    String pinned_dimension = nbt.getString("PinnedWorldRegistry");
                    RegistryKey<World> worldRegistryKey;
                    ServerWorld serverWorld;
                    if (pinned_dimension.contains("overworld")) {
                        worldRegistryKey = World.OVERWORLD;
                        serverWorld = ((ServerWorld) world).getServer().getWorld(worldRegistryKey);
                        if (serverWorld != null && getDayTime(realTime) == getDayTime(targetedTime)) {
                            entity.moveToWorld(serverWorld);
                            BlockPos pinnedPosition = readPositionFromNbt(nbt);
                            entity.requestTeleport(pinnedPosition.getX() + 0.5D, pinnedPosition.getY() + 1.0D, pinnedPosition.getZ() + 0.5D);
                        }
                    } else if (pinned_dimension.contains("the_nether")) {
                        worldRegistryKey = World.NETHER;
                        serverWorld = ((ServerWorld) world).getServer().getWorld(worldRegistryKey);
                        if (serverWorld != null && getDayTime(realTime) == getDayTime(targetedTime)) {
                            entity.moveToWorld(serverWorld);
                            BlockPos pinnedPosition = readPositionFromNbt(nbt);
                            entity.requestTeleport(pinnedPosition.getX() + 0.5D, pinnedPosition.getY() + 1.0D, pinnedPosition.getZ() + 0.5D);
                        }
                    } else if (pinned_dimension.contains("the_end")) {
                        worldRegistryKey = World.END;
                        serverWorld = ((ServerWorld) world).getServer().getWorld(worldRegistryKey);
                        if (serverWorld != null && getDayTime(realTime) == getDayTime(targetedTime)) {
                            entity.moveToWorld(serverWorld);
                            BlockPos pinnedPosition = readPositionFromNbt(nbt);
                            entity.requestTeleport(pinnedPosition.getX() + 0.5D, pinnedPosition.getY() + 1.0D, pinnedPosition.getZ() + 0.5D);
                        }
                    }
                }

                if (world.isClient()) {
                    if (getDayTime(realTime) == getDayTime(targetedTime)) {
                        entity.playSound(SoundEvents.BLOCK_END_PORTAL_SPAWN, 0.4F, 0.4F);
                    }
                }
            }
        }

        super.inventoryTick(stack, world, entity, slot, selected);
    }

    @Override
    public void appendTooltip(ItemStack stack, @Nullable World world, List<Text> tooltip, TooltipContext context) {
        String timeInToolTip = stack.getTranslationKey() + ".tooltip." + "3";
        tooltip.add(new TranslatableText(timeInToolTip, getDayTimeInClock(realTime)).formatted(Formatting.GOLD));
        //调试用
        if (JomosAdventure.ISDEBUG) {
            //显示记录的坐标
            String pinnedPosTip = stack.getTranslationKey() + ".tooltip." + "4";
            tooltip.add(new TranslatableText(pinnedPosTip, printPositionFromNbt(stack.getOrCreateNbt())).formatted(Formatting.BLUE));
            //显示当前的NBT
            String NBT = stack.getTranslationKey() + ".tooltip." + "5";
            tooltip.add(new TranslatableText(NBT, printNBT(stack.getOrCreateNbt())).formatted(Formatting.DARK_GREEN));
        }
        tooltip.add(new TranslatableText(stack.getTranslationKey() + ".tooltip." + "1").formatted(Formatting.DARK_AQUA).formatted(Formatting.ITALIC));
        tooltip.add(new TranslatableText(stack.getTranslationKey() + ".tooltip." + "2").formatted(Formatting.DARK_GRAY));
        if (stack.isItemEqual(new ItemStack(ModItems.TIMING_TELEPORT_GEM)) || stack.isItemEqual(new ItemStack(ModItems.NIGHTFALL_TELEPORT_GEM))) {
            tooltip.add(new TranslatableText(stack.getTranslationKey() + ".tooltip." + "6").formatted(Formatting.YELLOW));
            tooltip.add(new TranslatableText(stack.getTranslationKey() + ".tooltip." + "7").formatted(Formatting.WHITE));
        }
        if (stack.isItemEqual(new ItemStack(ModItems.BLESS_TELEPORT_GEM))) {
            if (!stack.getOrCreateNbt().getBoolean("isOnly")) {
                tooltip.add(new TranslatableText(stack.getTranslationKey() + ".tooltip." + "6").formatted(Formatting.YELLOW));
            }
        }
    }
}
