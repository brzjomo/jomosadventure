package com.brzjomo.jomosadventure.item;

import com.brzjomo.jomosadventure.JomosAdventure;
import com.brzjomo.jomosadventure.init.ModSoundEvents;
import net.minecraft.block.BlockState;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;
import net.minecraft.util.BlockRotation;
import net.minecraft.util.Formatting;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class TestItem extends Item {

    public TestItem(Settings settings) {
        super(settings);
    }

    @Override
    public ActionResult useOnBlock(ItemUsageContext context) {
        World world = context.getWorld();
        BlockState state = world.getBlockState(context.getBlockPos());
        world.setBlockState(context.getBlockPos(), state.rotate(BlockRotation.CLOCKWISE_90));
        if(world.isClient){
            context.getPlayer().playSound(SoundEvents.BLOCK_ANVIL_LAND, 1.0F, 1.0F);
//            context.getWorld().playSoundFromEntity(context.getPlayer(), context.getPlayer(), ModSoundEvents.NEUTRAL_CLOCK_TICK, SoundCategory.NEUTRAL, 1.0F, 0.4F);
        }
        context.getStack().damage(1, context.getPlayer(), (p) -> {
            p.sendToolBreakStatus(context.getHand());
        });
        //
//        if (JomosAdventure.ISDEBUG) {
//            JomosAdventure.LOGGER.info(context.getPlayer().getHealth());
//        }

        return ActionResult.SUCCESS;
    }

    @Override
    public void appendTooltip(ItemStack stack, @Nullable World world, List<Text> tooltip, TooltipContext context) {
        tooltip.add(new TranslatableText(stack.getTranslationKey() + ".tooltip." + "1").formatted(Formatting.DARK_AQUA).formatted(Formatting.ITALIC));
        tooltip.add(new TranslatableText(stack.getTranslationKey() + ".tooltip." + "2").formatted(Formatting.DARK_GRAY));
        tooltip.add(new TranslatableText(stack.getTranslationKey() + ".tooltip." + "3").formatted(Formatting.DARK_GRAY));
        tooltip.add(new TranslatableText(stack.getTranslationKey() + ".tooltip." + "4").formatted(Formatting.DARK_GRAY));
        tooltip.add(new TranslatableText(stack.getTranslationKey() + ".tooltip." + "5").formatted(Formatting.DARK_GRAY));
    }
}
