package com.brzjomo.jomosadventure.item;

import com.brzjomo.jomosadventure.JomosAdventure;
import com.brzjomo.jomosadventure.init.ModItems;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.Heightmap;
import net.minecraft.world.World;

import java.util.Objects;
import java.util.Random;

public class BlessTeleportGem extends TimingTeleportGem {

    public BlessTeleportGem(long time, Settings settings) {
        super(time, settings);
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        ItemStack itemStack = user.getStackInHand(hand);
        return TypedActionResult.pass(itemStack);
    }

    @Override
    public void inventoryTick(ItemStack stack, World world, Entity entity, int slot, boolean selected) {
        NbtCompound nbt = stack.getOrCreateNbt();

        if (!nbt.contains("isTriggered")) {
            nbt.putBoolean("isTriggered", false);
        }
        if (!nbt.contains("isSoundPlayed")) {
            nbt.putBoolean("isSoundPlayed", false);
        }
        if (!nbt.contains("shouldDamage")) {
            nbt.putBoolean("shouldDamage", false);
        }
        if (!nbt.contains("isOnly")) {
            nbt.putBoolean("isOnly", true);
        }

        // judge if multiple blessing gems exist
        DefaultedList<ItemStack> inventoryList = ((PlayerEntity) entity).currentScreenHandler.getStacks();
        int gemCount = 0;
        for (ItemStack itemStack : inventoryList) {
            if (itemStack.isItemEqual(new ItemStack(ModItems.BLESS_TELEPORT_GEM))) {
                gemCount++;
            }
        }
//        JomosAdventure.LOGGER.info("All: " + gemCount);
        nbt.putBoolean("isOnly", gemCount == 1);

        float maxHealth = 0;
        if (!entity.isSpectator() && entity.isAlive()) {
            maxHealth = Objects.requireNonNull(world.getClosestPlayer(entity, 1)).getMaxHealth();
        }
        float currentHealth = 0;
        if (!entity.isSpectator() && entity.isAlive()) {
            currentHealth = Objects.requireNonNull(world.getClosestPlayer(entity, 1)).getHealth();
        }
        float triggerPercent = 0.15F;
        if (currentHealth <= maxHealth * triggerPercent && currentHealth != 0 && !nbt.getBoolean("isTriggered") && nbt.getBoolean("isOnly")) {
            int maxDamage = stack.getMaxDamage();
            String player_dimension = entity.getWorld().getRegistryKey().toString();
            BlockPos currentPosition = Objects.requireNonNull(world.getClosestPlayer(entity, 1)).getBlockPos();
            if (!world.isClient() && nbt.getInt("Damage") < maxDamage) {
                if (player_dimension.contains("overworld")) {
                    Random random = new Random();
                    int targetX = currentPosition.getX() + random.nextInt(-800, 800);
                    int targetZ = currentPosition.getY() + random.nextInt(-800, 800);
                    int targetY = world.getTopY(Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, targetX, targetZ);
                    BlockPos targetPos = new BlockPos(targetX, targetY, targetZ);
                    while (!world.getBlockState(targetPos).equals(Blocks.AIR.getDefaultState()) || (world.getBlockState(targetPos).equals(Blocks.AIR.getDefaultState()) && !world.getBlockState(new BlockPos(targetX, targetY + 1, targetZ)).equals(Blocks.AIR.getDefaultState()))) {
                        targetY += 1;
                        targetPos = new BlockPos(targetX, targetY, targetZ);
                    }
                    entity.requestTeleport(targetX + 0.5D, targetY + 1.0D, targetZ + 0.5D);
                    nbt.putBoolean("isTriggered", true);
                    nbt.putBoolean("shouldDamage", true);
                } else if (player_dimension.contains("the_nether")) {
                    Random random = new Random();
                    int targetX = 0;
                    int targetZ = 0;
                    int targetY = 0;
                    BlockPos targetPos = new BlockPos(targetX, targetY, targetZ);
                    boolean isGoDown = false;
                    while (targetPos.equals(new BlockPos(0, 0, 0)) || world.getBlockState(new BlockPos(targetX, targetY - 2, targetZ)).equals(Blocks.LAVA.getDefaultState()) || world.getBlockState(new BlockPos(targetX, targetY - 2, targetZ)).equals(Blocks.BEDROCK.getDefaultState())) {
                        targetX = currentPosition.getX() + random.nextInt(-200, 200);
                        targetZ = currentPosition.getY() + random.nextInt(-200, 200);
                        targetY = world.getTopY(Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, targetX, targetZ);
                        targetPos = new BlockPos(targetX, targetY, targetZ);
                        while (!world.getBlockState(targetPos).equals(Blocks.AIR.getDefaultState())) {
                            targetY += 1;
                            targetPos = new BlockPos(targetX, targetY, targetZ);
                        }
                        if (world.getBlockState(new BlockPos(targetX, targetY - 1, targetZ)).equals(Blocks.BEDROCK.getDefaultState())) {
                            isGoDown = true;
                            targetY -= 1;
                            targetPos = new BlockPos(targetX, targetY, targetZ);
                        }
                        while (isGoDown && !world.getBlockState(targetPos).equals(Blocks.AIR.getDefaultState()) || (world.getBlockState(targetPos).equals(Blocks.AIR.getDefaultState()) && !world.getBlockState(new BlockPos(targetX, targetY - 1, targetZ)).equals(Blocks.AIR.getDefaultState())) || (world.getBlockState(targetPos).equals(Blocks.AIR.getDefaultState()) && world.getBlockState(new BlockPos(targetX, targetY - 1, targetZ)).equals(Blocks.AIR.getDefaultState()) && world.getBlockState(new BlockPos(targetX, targetY - 2, targetZ)).equals(Blocks.AIR.getDefaultState()))) {
                            targetY -= 1;
                            targetPos = new BlockPos(targetX, targetY, targetZ);
                        }
                    }
                    entity.requestTeleport(targetX + 0.5D, targetY + 1.0D, targetZ + 0.5D);
                    nbt.putBoolean("isTriggered", true);
                    nbt.putBoolean("shouldDamage", true);
                } else if (player_dimension.contains("the_end")) {
                    Random random = new Random();
                    int targetX = 0;
                    int targetZ = 0;
                    int targetY = 0;
                    BlockPos targetPos = new BlockPos(targetX, targetY, targetZ);
                    boolean isVoid = true;
                    while (isVoid) {
                        targetX = currentPosition.getX() + random.nextInt(-100, 100);
                        targetZ = currentPosition.getZ() + random.nextInt(-100, 100);
                        targetY = world.getTopY(Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, targetX, targetZ);
                        targetPos = new BlockPos(targetX, targetY, targetZ);
                        if (!world.getBlockState(new BlockPos(targetX, targetY - 1, targetZ)).equals(Blocks.AIR.getDefaultState()) && targetY != 0) {
                            isVoid = false;
                            while (!world.getBlockState(targetPos).equals(Blocks.AIR.getDefaultState()) || (world.getBlockState(targetPos).equals(Blocks.AIR.getDefaultState()) && !world.getBlockState(new BlockPos(targetX, targetY + 1, targetZ)).equals(Blocks.AIR.getDefaultState()))) {
                                targetY += 1;
                                targetPos = new BlockPos(targetX, targetY, targetZ);
                            }
                        }
                    }
                    entity.requestTeleport(targetX + 0.5D, targetY + 1.0D, targetZ + 0.5D);
                    nbt.putBoolean("isTriggered", true);
                    nbt.putBoolean("shouldDamage", true);
                }
            }
        }

        if (nbt.getBoolean("isTriggered") && !nbt.getBoolean("isSoundPlayed")) {
            if (world.isClient()) {
                entity.playSound(SoundEvents.BLOCK_END_PORTAL_SPAWN, 0.4F, 0.4F);
                nbt.putBoolean("isSoundPlayed", true);
            }
        }

        if (nbt.getBoolean("shouldDamage")) {
            int maxDamage = stack.getMaxDamage();
            nbt.putInt("Damage", nbt.getInt("Damage") + 1);
            // 耐久为0时摧毁物品
            if (nbt.getInt("Damage") >= maxDamage) {
                stack.decrement(1);
            } else {
                nbt.putBoolean("shouldDamage", false);
            }
        }

        if (entity.isOnFire() && nbt.getBoolean("isTriggered")) {
            entity.setFireTicks(-60);
        }

        if (currentHealth > maxHealth * triggerPercent && nbt.getBoolean("isTriggered") || !entity.isAlive()) {
            nbt.putBoolean("isTriggered", false);
            nbt.putBoolean("isSoundPlayed", false);
        }

        super.inventoryTick(stack, world, entity, slot, selected);
    }
}
