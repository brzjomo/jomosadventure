package com.brzjomo.jomosadventure;

import com.brzjomo.jomosadventure.init.ModBlocks;
import com.brzjomo.jomosadventure.init.ModFeatures;
import com.brzjomo.jomosadventure.init.ModItems;
import com.brzjomo.jomosadventure.init.ModSoundEvents;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.biome.v1.BiomeModifications;
import net.fabricmc.fabric.api.biome.v1.BiomeSelectors;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.sound.SoundEvent;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.BuiltinRegistries;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.gen.GenerationStep;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class JomosAdventure implements ModInitializer {
    public static final String MOD_ID = "jomosadventure";
    public static final Logger LOGGER = LogManager.getLogger(MOD_ID);
    public static final boolean ISDEBUG = false;

    @Override
    public void onInitialize() {
        // Register Blocks
        for (Block i : ModBlocks.BLOCK_LIST) {
            Registry.register(Registry.BLOCK, ModBlocks.getId(i), i);
        }

        // Register BlockItems
        for (Item i : ModItems.BLOCKITEM_LIST) {
            Registry.register(Registry.ITEM, ModItems.getId(i), i);
        }

        // Register Items
        for (Item i : ModItems.ITEM_LIST) {
            Registry.register(Registry.ITEM, ModItems.getId(i), i);
        }

        // Register Sounds
        for (SoundEvent i : ModSoundEvents.SOUND_EVENTS_LIST) {
            Registry.register(Registry.SOUND_EVENT, new Identifier(JomosAdventure.MOD_ID, ModSoundEvents.getPath(i)), i);
        }

        // Register Ore Configured Feature For Overworld
        for (int i = 0; i < ModFeatures.ORE_CONFIGURED_FEATURE_KEY_LIST_OVERWORLD.length; i++) {
            Registry.register(BuiltinRegistries.CONFIGURED_FEATURE, ModFeatures.ORE_CONFIGURED_FEATURE_KEY_LIST_OVERWORLD[i].getValue(), ModFeatures.ORE_CONFIGURED_FEATURE_LIST_OVERWORLD[i]);
        }

        // Register Ore Placed Feature For Overworld
        for (int i = 0; i < ModFeatures.ORE_PLACED_FEATURE_KEY_LIST_OVERWORLD.length; i++) {
            Registry.register(BuiltinRegistries.PLACED_FEATURE, ModFeatures.ORE_PLACED_FEATURE_KEY_LIST_OVERWORLD[i].getValue(), ModFeatures.ORE_PLACED_FEATURE_LIST_OVERWORLD[i]);
        }

        // Register Ores In Overworld
        for (RegistryKey i : ModFeatures.ORE_PLACED_FEATURE_KEY_LIST_OVERWORLD) {
            BiomeModifications.addFeature(BiomeSelectors.foundInOverworld(), GenerationStep.Feature.UNDERGROUND_ORES, i);
        }

    }

}
