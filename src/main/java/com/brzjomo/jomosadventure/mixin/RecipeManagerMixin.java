package com.brzjomo.jomosadventure.mixin;

import com.brzjomo.jomosadventure.JomosAdventure;
import com.brzjomo.jomosadventure.init.ModBlocks;
import com.brzjomo.jomosadventure.init.ModItemRecipes;
import com.brzjomo.jomosadventure.init.ModItems;
import com.google.gson.JsonElement;
import net.minecraft.item.Items;
import net.minecraft.recipe.RecipeManager;
import net.minecraft.resource.ResourceManager;
import net.minecraft.util.Identifier;
import net.minecraft.util.profiler.Profiler;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Map;

@Mixin(RecipeManager.class)
public class RecipeManagerMixin {
    @Inject(method = "apply", at = @At("HEAD"))
    public void interceptApply(Map<Identifier, JsonElement> map, ResourceManager resourceManager, Profiler profiler, CallbackInfo info) {
        map.put(new Identifier(JomosAdventure.MOD_ID, ModItems.TEST_ITEM_PATH), ModItemRecipes.TEST_ITEM_RECIPE);
        map.put(new Identifier(JomosAdventure.MOD_ID, ModItems.TEST_2_ITEM_PATH), ModItemRecipes.TEST_2_ITEM_RECIPE);
        // Silver
        map.put(new Identifier(JomosAdventure.MOD_ID, ModItems.SILVER_NUGGET_PATH), ModItemRecipes.SILVER_NUGGET);
        map.put(new Identifier(JomosAdventure.MOD_ID, ModItems.SILVER_INGOT_PATH), ModItemRecipes.SILVER_INGOT_SMELTING);
        map.put(new Identifier(JomosAdventure.MOD_ID, ModItems.SILVER_INGOT_PATH + "_from_nugget"), ModItemRecipes.SILVER_INGOT_FROM_NUGGET);
        map.put(new Identifier(JomosAdventure.MOD_ID, ModItems.SILVER_INGOT_PATH + "_from_block"), ModItemRecipes.SILVER_INGOT);
        map.put(new Identifier(JomosAdventure.MOD_ID, ModBlocks.SILVER_BLOCK_PATH), ModItemRecipes.SILVER_BLOCK);
        // Mithril
        map.put(new Identifier(JomosAdventure.MOD_ID, ModItems.MITHRIL_NUGGET_PATH), ModItemRecipes.MITHRIL_NUGGET);
        map.put(new Identifier(JomosAdventure.MOD_ID, ModItems.MITHRIL_INGOT_PATH + "_from_nugget"), ModItemRecipes.MITHRIL_INGOT_FROM_NUGGET);
        map.put(new Identifier(JomosAdventure.MOD_ID, ModItems.MITHRIL_INGOT_PATH + "_from_nugget_2"), ModItemRecipes.MITHRIL_INGOT_FROM_NUGGET_2);
        map.put(new Identifier(JomosAdventure.MOD_ID, ModItems.MITHRIL_INGOT_PATH + "_from_block"), ModItemRecipes.MITHRIL_INGOT);
        map.put(new Identifier(JomosAdventure.MOD_ID, ModBlocks.MITHRIL_BLOCK_PATH), ModItemRecipes.MITHRIL_BLOCK);
        //Item
        map.put(new Identifier(JomosAdventure.MOD_ID, ModItems.NIGHTFALL_TELEPORT_GEM_PATH), ModItemRecipes.NIGHTFALL_TELEPORT_GEM);
        map.put(new Identifier(JomosAdventure.MOD_ID, ModItems.BLESS_TELEPORT_GEM_PATH), ModItemRecipes.BLESS_TELEPORT_GEM);
        //Staff
        map.put(new Identifier(JomosAdventure.MOD_ID, ModItems.STAFF_OF_DESIGNATOR_PATH), ModItemRecipes.STAFF_OF_DESIGNATOR);
        //Additional Recipe For Vanilla Object
        map.put(new Identifier("minecraft", "shears_from_copper_ingot"), ModItemRecipes.SHEARS_FROM_COPPER_INGOT);
        map.put(new Identifier("minecraft", "bucket_from_copper_ingot"), ModItemRecipes.BUCKET_FROM_COPPER_INGOT);
        map.put(new Identifier("minecraft", "hopper_from_copper_ingot"), ModItemRecipes.HOPPER_FROM_COPPER_INGOT);
        //Slope_Block
        map.put(new Identifier(JomosAdventure.MOD_ID, ModBlocks.SLOPE_STONE_PATH), ModItemRecipes.SLOPE_STONE_FROM_STONECUTTING);
    }
}
